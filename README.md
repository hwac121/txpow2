# txpow2
Version 2.0 of TX Power Blast with enhanced features

Description
-
This is version 2.0 of TX Power Blast written in the bash shell.
This program will boost your transmit power of your chosen WiFi antenna to 30db
and open all channels by changing the region of the antenna fron US to a country
that allows more operational channels.

Features
-
Raise TX Power to 30db
Open all channels
Check current WiFi transmit power
Get internal IP
Get external IP

Disclaimer
-
This software is under a public open source license and is released for educational
purposes only. It would be illegal to use this software in mainstream use without the
confines of a lab. I am not responsible for the method of use by the end-user. I will
assume no legally responsibility for how anyone chooses to use this software.

Release Information
-
This software may be copied in whole or part for any use whatever as long as it includes
the author information, given credit for said code.

Donations
-
Donations may be sent via PayPal using the email rjlemail@gmail.com

INSTALLATION
-
1. git clone https://github.com/hwac121/txpow2.git
2. cd txpow2
3. chmod +x install.sh
4. sudo ./install.sh

(***NOTE:*** When running step four either run as root ./install.sh or sudo if not root.)
