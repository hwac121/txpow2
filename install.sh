#!/bin/bash

#================================================================================================
#                                    LISTED VARIABLES
#================================================================================================


#================================================================================================
#========                          CHECK IF ROOT OR EXIT                                ========#
#================================================================================================

if [ "$EUID" -ne 0 ]
  then 
  echo "You must run as root or sudo txpow"
  exit
fi

#================================================================================================
#                                    CLEAR SCREEN TO BEGIN
#================================================================================================

clear
echo -e "\e[32m ______ __ __ ____   ___  __    __   ___ ____       ____  _      ____  ___________ \e[0m";
sleep 0.05
echo -e "\e[32m|      |  |  |    \ /   \|  |__|  | /  _]    \     |    \| |    /    |/ ___/      |\e[0m";
sleep 0.05
echo -e "\e[32m|      |  |  |  o  )     |  |  |  |/  [_|  D  )    |  o  ) |   |  o  (   \_|      |\e[0m";
sleep 0.05
echo -e "\e[32m|_|  |_|_   _|   _/|  O  |  |  |  |    _]    /     |     | |___|     |\__  |_|  |_|\e[0m";
sleep 0.05
echo -e "\e[32m  |  | |     |  |  |     |  \`  '  |   [ |    \     |  O  |     |  _  |/  \ | |  |  \e[0m";
sleep 0.05
echo -e "\e[32m  |  | |  |  |  |  |     |\      /|     |  .  \    |     |     |  |  |\    | |  |  \e[0m";
sleep 0.05
echo -e "\e[32m  |__| |__|__|__|   \___/  \_/\_/ |_____|__|\_|    |_____|_____|__|__| \___| |__|  \e[0m";
sleep 0.05
echo -e "\e[32m                                                                                   \e[0m";
sleep 0.05
echo -e "\e[35mVersion 2.0\e[0m";
sleep 0.05
echo -e "......................................................................................"
sleep 0.75

#================================================================================================
#                              CHECK FOR PREVIOUS VERSION
#================================================================================================

echo -e "Checking for previous installation..."
sleep 1.5
if [ -d $Dir ];
	then
		echo -e "Removing previous installation..."
		find $( ../txpow_blast/ ) && rm -r ../txpow_blast/;
		echo -e "Previous version removed"
	else
		echo -e "No previous installation found..."
		sleep 1.5
		echo -e "moving on..."
		sleep 1.5
fi

#================================================================================================
#                                 CHECK IF ALREADY INSTALLED
#================================================================================================

echo -e "Checking for current TX Power Blast installation..."
sleep 1.5
if [ -e /bin/txpow ]; then
  echo -e "TX Power Blast is already installed"
  sleep 1.5
  else
  echo "TX Power Blast not installed. Attempting to install TX Power Blast now..."
  sleep 1.5
  cp txpow.sh /bin/txpow
  sudo chmod +x /bin/txpow
  sudo chmod +x txpow.sh
  sleep 1.5
fi
clear
echo -e "Installation/Upgrade is complete!"
sleep 1.5
echo -e "Just type \e[32mtxpow\e[0m as root, or as user \e[32msudo txpow\e[0m to run anywhere in terminal"
sleep 4
clear

